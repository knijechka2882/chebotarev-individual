--1.Пользовательский интерфейс главной страницы и товара:
--Можем оценить количество кликов по основным элементам 

SELECT COUNT(*) AS gwb_banner_clicks
FROM actions_gwb
WHERE element_name = 'GWB_Main_Banner' AND action_id = 2;

--Сравнить конверсию перехода из банера в покупку

WITH gwb_banner_to_cart AS (
  SELECT user_id, session_id
  FROM actions_gwb
  WHERE element_name = 'GWB_Main_Banner' 
  AND user_id IN (SELECT user_id FROM actions_gwb WHERE action_id = 'add_to_cart'))

SELECT 'GWB' AS platform, COUNT(*) * 1.0 / (SELECT COUNT(*) FROM actions_gwb WHERE element_name = 'GWB_Main_Banner') AS conversion_rate
FROM gwb_banner_to_cart;

-- 2.Доступность к основным разделам категорий: 

--Посчитать количество пользователей использующий основные категории и возможно добавление товара в корзину с этой страницы

SELECT COUNT(*) AS gwb_add_cart_category
FROM actions_gwb
WHERE element_name = 'GWB_Add_to_Cart' AND session_id IN 
    (SELECT session_id FROM actions_gwb WHERE element_name = 'GWB_Category' AND action_id = 2);

-- 3. Ценообразование:

--Посчитать конверсии в добавление в корзину(оплату) для не авторизованных пользователей. 

WITH product_viewers AS (
    SELECT DISTINCT user_id
    FROM actions_wb
    WHERE element_name = 'WB_Product_Page' AND user_id NOT IN (SELECT DISTINCT user_id FROM actions_wb WHERE element_name = 'WB_Authorization')),

–Считаем количество уникальных пользователей, добавивших товар в корзину после просмотра

add_to_cart_users AS (
    SELECT DISTINCT user_id
    FROM actions_wb
    WHERE element_name = 'WB_Add_To_Cart' AND user_id NOT IN (SELECT DISTINCT user_id FROM actions_wb WHERE element_name = 'WB_Authorization') 
  	AND user_id IN (SELECT user_id FROM product_viewers))

–Вычисляем конверсию

SELECT 
    COUNT(DISTINCT user_id)::DECIMAL / NULLIF((SELECT COUNT(DISTINCT user_id) FROM product_viewers), 0) AS conversion_rate
FROM add_to_cart_users;

-- 4. Валюты для оплаты

--Можно посчитать количество транзакций или отдельно прибыль по валютам.

SELECT currency, COUNT(DISTINCT action_id) AS transactions_count
FROM actions_wb 
WHERE element_name = 'Payment_Process'
GROUP BY currency;


-- 5. Дополнительные разделы в выпадающем меню: 

--Можно рассчитать активность по разделам или  после посчитать конверсию

SELECT element_name, COUNT(DISTINCT action_id) AS clicks_count
FROM actions_wb
WHERE element_name IN ('Menu_Actions', 'Menu_Travels', 'Menu_DigitalGoods')
GROUP BY element_name;
	
-- 6. Авторизация и интерфейс профиля:
		
--Активность пользователей после авторизации:

SELECT COUNT(DISTINCT action_id) AS balance_clicks_count
FROM actions_wb
WHERE element_name = 'Balance_Icon';

--Использование дополнительных вкладок профиля

SELECT element_name, COUNT(DISTINCT action_id) AS tab_clicks_count
FROM actions_wb
WHERE element_name IN ('Tab_Receipts', 'Tab_FavoriteBrands')
GROUP BY element_name;
	
--Подсчитать, сколько пользователей, посещавших вкладку "чеки" или "любимые бренды", совершили покупку

WITH profile_tab_users AS (
    SELECT DISTINCT user_id
    FROM actions_wb
    WHERE element_name IN ('Tab_Receipts', 'Tab_FavoriteBrands')
)

SELECT COUNT(DISTINCT a.user_id) AS converted_users_count
FROM actions_wb a
JOIN profile_tab_users p ON a.user_id = p.user_id
WHERE a.element_name = 'Purchase_Complete';


-- 7. Информация о доставке в корзине: 
-- Так как эти кнопки не кликабельны, возможно, никаких данных о них не содержалось в таблице, 
--поэтому мы можем лишь в целом сравнить конверсии из добавлений в корзину в копки. Возможно провести A/B, чтобы оценить влияние.

-- 8. Способ оплаты

--Сравнить, какие способы оплаты выбирают пользователи на WB и GWB.

SELECT element_name AS payment_method, COUNT(DISTINCT action_id) AS method_clicks_count
FROM actions_gwb
WHERE element_name LIKE 'PaymentMethod%'
GROUP BY element_name;

--Сравнить, какие способы оплаты чаще всего приводят к завершению покупки.

WITH payment_selections_gwb AS (
    SELECT DISTINCT user_id, element_name AS payment_method
    FROM actions_gwb
    WHERE element_name LIKE 'PaymentMethod%')

SELECT p.payment_method, COUNT(DISTINCT a.user_id) AS conversion_count_gwb
FROM actions_gwb a
JOIN payment_selections_gwb p ON a.user_id = p.user_id
WHERE a.element_name = 'Purchase_Complete'
GROUP BY p.payment_method;


-- 9. Скорость работы

--Количество отказов после попытки взаимодействия с медленно загружающимися разделами

SELECT COUNT(DISTINCT user_id) AS dropoff_users_count
FROM actions_gwb
WHERE element_name = 'Profile_Purchases'
AND action_id NOT IN (
    SELECT DISTINCT action_id
    FROM actions_gwb
    WHERE action_datetime  > (SELECT action_datetime FROM actions_gwb WHERE element_name = 'Profile_Purchases'))
AND user_id IN (
    SELECT DISTINCT user_id
    FROM actions_gwb
    WHERE element_name = 'Profile_Purchases');

-- 10. Дополнительные условия и сборы

-- Посчитать, как часто пользователи отказываются от покупок на обеих платформах, чтобы понять.

SELECT COUNT(DISTINCT action_id) AS total_refunds_gwb
FROM actions_gwb
WHERE element_name = 'Refund';

-- Рассчитать потери (прибыль) от отказов.

SELECT SUM(profit) AS lost_profit_gwb
FROM actions_gwb
WHERE element_name = 'Refund';

